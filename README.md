ics-ans-role-notebook
=====================

WARNING! This role is deprecated.
It was only used to build a docker image (using packer).
Dockerfiles are now used. See "ics-docker-jupytr-notebook" repository.

Ansible role to install jupyter notebook.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
notebook_tini_version: v0.14.0
notebook_julia_version: v0.6
notebook_julia_packages:
  - AverageShiftedHistograms
  - Distributions
  - IJulia
  - ImageView
  - Images
  - Interact
  - Interpolations
  - LaTeXStrings
  - MultiPoly
  - MultivariateStats
  - NearestNeighbors
  - Plotly
  - PlotlyJS
  - PyCall
  - PyPlot
  - Plots
  - QuartzImageIO
  - StatPlots
  - StatsBase
  - SymPy
  - TaylorSeries
notebook_epics_base: /opt/epics/bases/base-3.15.4
notebook_epics_host_arch: centos7-x86_64
notebook_epics_libs_archive: https://artifactory.esss.lu.se/artifactory/ansible-provision/epics-libs/3.15.4/centos7-x86_64/epics_libs.tgz
notebook_app_iopub_data_rate_limit: 0
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-notebook
```

License
-------

BSD 2-clause
