How to create the root.yml and python2.yml files
================================================

root.yml
--------

::

    $ conda update conda
    $ conda install python=3.6 \
      ase \
      bokeh \
      bz2file \
      cython \
      graphviz \
      hdf5 \
      h5py \
      holoviews \
      jpype1 \
      jsonschema \
      jupyter \
      jupyterhub \
      jupyter_cms \
      jupyter_dashboards \
      matplotlib \
      nbexamples \
      networkx \
      numba \
      numexpr \
      openpyxl \
      oauthlib \
      pandas \
      pandoc \
      pandocfilters \
      pillow \
      pymongo \
      python-graphviz \
      requests \
      requests-oauthlib \
      scipy \
      seaborn \
      sympy \
      xlrd \
      xlwt
    
    $ pip install jira pyepics pygraphviz pypandoc pydot jupyter-dashboards-bundlers
    $ conda env export -n root > root.yml


python2.yml
-----------

::

    $ conda create -c mantid -n python2 python=2.7 \
      ase \
      bokeh \
      bz2file \
      cython \
      graphviz \
      hdf5 \
      h5py \
      holoviews \
      jpype1 \
      jsonschema \
      jupyter \
      jupyter_cms \
      jupyter_dashboards \
      mantid-framework \
      matplotlib \
      networkx \
      numba \
      numexpr \
      openpyxl \
      oauthlib \
      pandas \
      pandoc \
      pandocfilters \
      pillow \
      pydot \
      pymongo \
      pypandoc \
      python-graphviz \
      requests \
      requests-oauthlib \
      scipy \
      seaborn \
      sympy \
      xlrd \
      xlwt
    
    $ source activate python2
    $ pip install jira pyepics pygraphviz jupyter-dashboards-bundlers
    $ conda env export > python2.yml
